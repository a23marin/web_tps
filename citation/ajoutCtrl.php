<?php
require("./utils/inputs.php");
require_once("Entity/Author.class.php");
include_once("./Entity/Citation.class.php");
include "init.php";
include "utils/citations.php";

$errors = false;
function safeGetValue($field)
{
    if (isset($field)) {
        return getValue($field);
    }
    return null;
}

function memorizeValue(string $field)
{
    isset($_POST["login"]) ? getValue($_POST["login"]) : "";
}


ini_set("display_errors", "1");
ini_set("display_startup_errors", "1");
error_reporting(E_ALL);

function getError(string $field)
{

    if (!empty($_POST)) {

        if (!checkValue($_POST[$field])) {
            $errors = true;
            return "champ vide";
        }
        if ($field === "date") {
            if (!validateDate(getValue($_POST["date"]))) {
                $errors = true;
                return "format de date incorrecte";
            }
        }
    }

}

function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

function getFieldValue(string $field)
{

}


// init

$system = init();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ajout de citation </title>
    <meta charset="UTF-8">
    <style type="text/css">
        .error {
            font-style: italic;
            color: red;
        }

        label {

            display: flex;

        }

        label>bold {
            display: inline-block;
            width: 15rem;
        }

        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }



        .citation {
            display: flex;
            flex-direction: column;
            gap: 1rem;
            margin: 0.5rem 1rem;
            padding: 0.5rem 1rem;
            /* border: solid 1px black; */
            width: fit-content;
            background-color: lightgrey
        }

        p {
            margin: 0;

        }

        .box {
            padding: 1rem 2rem;
            border: solid 1px black;

        }

        .container {
            margin: auto;
            with: 80%;
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem;

        }
    </style>

    <script>

        const toggleAuthorFields = (event) => {
            const fieldsContainer = document.getElementById("author_fields");
            const originalField = document.getElementById("author_input");
            if (event.target.checked) {
                originalField.style = "display: none;"
                fieldsContainer.style = "display: box;"
            } else {
                fieldsContainer.style = "display: none;"
                originalField.style = "display: box;"
            }


        }

        toggleAuthorFields();

    </script>



</head>

<body>
    <main>
        <article>
            <header>
                <h1>Formaire de création de citations</h1>
            </header>

            <form method="post" name="FrameCitation" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <table border="1" bgcolor="#ccccff" frame="above">
                    <tbody>
                        <tr>
                            <th><label for="login">Login</label></th>
                            <td><input name="login" maxlength="64" size="32"
                                    value="<?php echo memorizeValue("login") ?>"></td>
                            <td><span class="error">
                                    <?php echo getError("login") ?>
                                </span></td>
                        </tr>
                        <tr>
                            <th><label for="citation">Citation</label></th>
                            <td><textarea cols="128" rows="5" name="citation"
                                    value="<?php echo memorizeValue("citation") ?>"></textarea></td>
                            <td><span class="error">
                                    <?php echo getError("citation") ?>
                                </span></td>

                        </tr>
                        <tr>
                            <th><label for="auteur">Auteur</label></th>
                            <td>
                                <input id="author_input" name="author" maxlength="128" size="64"
                                    value="<?php echo memorizeValue("author") ?>" list="authors">
                                <datalist id="authors">
                                    <?php
                                    foreach ($system->authors as $author) {
                                        # code...
                                        echo "<option value=\"{$author->lastName}, {$author->name}\">";
                                    }
                                    ?>
                                </datalist>
                                <label>
                                    <bold>
                                        Auteur pas dans liste:
                                    </bold>
                                    <input type="checkbox" name="newAuthor" value="newAuthor"
                                        onchange="toggleAuthorFields(event)">
                                </label>
                                <div id="author_fields" style="display:none">
                                    <label>
                                        <bold>Prénom:</bold>
                                        <input id="author_name" name="author_name" maxlength="128" size="64">
                                    </label>
                                    <label>
                                        <bold>Nom: </bold>
                                        <input id="author_last_name" name="author_last_name" maxlength="128" size="64">
                                    </label>
                                    <label>
                                        <bold>Année de naissance:</bold>
                                        <input id="author_year" name="author_year" maxlength="128" size="64">
                                    </label>

                                </div>
                            </td>
                            <td><span class="error">
                                    <?php echo getError("author") ?>
                                </span></td>

                        </tr>
                        <tr>
                            <th><label for="date">Date</label></th>
                            <td><input name="date" maxlength="128" size="64" type="date"
                                    value="<?php echo memorizeValue("date") ?: Date("Y-m-d"); ?>"></td>
                            <td><span class="error">
                                    <?php echo getError("date") ?>
                                </span></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input name="Envoyer" value="Enregistrer la citation" type="submit">
                                <input name="Effacer" value="Anner" type="reset">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </article>
        <section>
            <?php
            if (!empty($_POST) and !$errors) {

                $citation = new Citation(getValue($_POST["login"]), getValue($_POST["citation"]), getValue($_POST["date"]));

                if (isset($_POST["newAuthor"])) {

                    $author = new Author(getValue($_POST["author_name"]), getValue($_POST["author_last_name"]), getValue($_POST["author_year"]), [$citation]);
                    // array_push($system->authors, $author);
                    $system->authors[] = $author;
                } else {
                    $i = 0;
                    foreach ($system->authors as $author_item) {
                        # code...
                        if ($author_item->isEqualByName(getValue($_POST["author"]))) {
                            // array_push($system->authors[$i]->citations, $citation);
                            // $author_item->citations[] = $citation;
                            $author_item->addCitation($citation);
                            // $system->authors[$i]->citations[] = $citation;
                        }
                        $i++;
                    }
                }

                foreach ($system->authors as $author) {
                    # code...
                    echo displayAuthor($author);
                }
            }
            ?>
        </section>


    </main>
</body>

</html>
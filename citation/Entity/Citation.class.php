<?php


class Citation
{
    public function __construct(
        public string $login,
        public string $citation,
        public string $date,
        public ?int $id = null,
        public ?int $author = null,
        public ?string $creationDate = null,
    )
    {
    }
    public static function fromArray(
        array $data
    )
    {
        // return new self(...$data);
        return new self($data["login"], $data["citation"], $data["date"], $data["id"] ?: null, $data["author_id"] ?: null, $data["creationDate"] ?: null);
    }
    public static function toArray(
        Citation $data
    )
    {
        return ["login" => $data->login, "citation" => $data->citation, "date" => $data->date, "id" => $data->id, "author" => $data->author, "creationDate" => $data->creationDate,];
    }
}
;


?>
<?php
include_once "Citation.class.php";
class Author
{
    public function __construct(
        public string $name,
        public string $lastName,
        public int $birthYear,
        public array $citations,
    )
    {

    }
    public function addCitation(Citation $citation)
    {
        $this->citations[] = $citation;
    }
    public function isEqual(Author $author)
    {
        return ($this->name == $author->name) and ($this->lastName == $author->lastName) and ($this->birthYear == $author->birthYear);
    }

    public function isEqualByName(string $fullname): bool
    {
        return "{$this->lastName}, {$this->name}" == $fullname;
    }

    public static function toArray(Author $data): array
    {
        return ["name" => $data->name, "lastName" => $data->lastName, "birthYear" => $data->birthYear, "citation" => array_map(fn($item) => Citation::toArray($item), $data->citations)];
    }
}
;


?>
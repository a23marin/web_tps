<?php


include_once("Entity/CitationSystem.class.php");
include_once("Entity/Author.class.php");

function init(): CitationSystem
{
    $authorsAmount = 2;
    $citationsAmount = 1;

    $authors = [];

    for ($i = 0; $i < $authorsAmount; $i++) {
        # code...
        $citations = array();
        for ($j = 0; $j < $citationsAmount; $j++) {
            # code...
            $citationDate = date("Y-m-d", mktime(1, 2, 3, $i, $j, 2023));
            $citation = new Citation("login $i $j", "lore ipsum dolum $i $j", $citationDate);

            array_push($citations, $citation);

        }
        // echo "Amount of citations for author $i: " . count($citations) . "\n";
        $author = new Author("Auteur_$i", "Nom_$i", 1930 + $i, $citations);
        array_push($authors, $author);
    }
    // echo "\nAuthors Lnegth:" . count($authors) . "\n";
    return new CitationSystem($authors);
}

?>
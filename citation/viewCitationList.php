<?php
include "utils/db-functions.php";

function displayCitationRow(array $citation): string
{
    $markup = "
    <tr>
        <td>{$citation["login"]}</td>
        <td>{$citation["author"]}</td>
        <td>{$citation["date"]}</td>
        <td>{$citation["creationDate"]}</td>
        <td><a href=\"viewCitation.php?id={$citation["id"]}\" target=\"_blank\" rel=\"noopener noreferrer\">voir citation</a></td>
    </tr>\n";
    return $markup;
}

// connect to db

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Citation List</title>
</head>

<body>
    <main>
        <table>
            <tr>
                <th>Login</th>
                <th>Auteur</th>
                <th>Date de citation</th>
                <th>Date d’enregistrement</th>
                <th>Lire</th>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="viewCitation.php?id=" target="_blank" rel="noopener noreferrer"></a></td>
            </tr>

            <?php
            $citationModel = new CitationModel();
            $citations = $citationModel->find();

            foreach ($citations as $citation) {
                echo displayCitationRow(Citation::toArray($citation));
            }
            ?>

        </table>
    </main>
</body>

</html>
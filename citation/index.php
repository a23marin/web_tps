<?php
include "utils/db-functions.php";

function displayCitationRow(array $citation): string
{
    $markup = "
    <tr>
        <td>{$citation["login"]}</td>
        <td>{$citation["author"]}</td>
        <td>{$citation["date"]}</td>
        <td>{$citation["creationDate"]}</td>
        <td><a href=\"viewCitation.php?id={$citation["id"]}\" target=\"_blank\" rel=\"noopener noreferrer\">voir citation</a></td>
    </tr>\n";
    return $markup;
}

// connect to db

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
</head>

<body>
    <main>
        <table>
            <tr>
                <th>Login</th>
                <th>Auteur</th>
                <th>Date de citation</th>
                <th>Date d’enregistrement</th>
                <th>Lire</th>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="viewCitation.php?id=" target="_blank" rel="noopener noreferrer"></a></td>
            </tr>

            <?php
            $citationModel = new CitationModel();
            $result = $citationModel->execute("SELECT * FROM `citation` WHERE creationDate > now() - INTERVAL 5 day ORDER BY creationDate DESC")->fetchAll();
            $citations = array_map(fn($item) => Citation::fromArray($item), $result);

            foreach ($citations as $citation) {
                echo displayCitationRow(Citation::toArray($citation));
            }
            ?>

        </table>
    </main>
</body>

</html>
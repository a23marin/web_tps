<?php

include "init.php";
include "utils/citations.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }



        .citation {
            display: flex;
            flex-direction: column;
            gap: 1rem;
            margin: 0.5rem 1rem;
            padding: 0.5rem 1rem;
            /* border: solid 1px black; */
            width: fit-content;
            background-color: lightgrey
        }

        p {
            margin: 0;

        }

        .box {
            padding: 1rem 2rem;
            border: solid 1px black;

        }

        .container {
            margin: auto;
            with: 80%;
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem;

        }
    </style>
</head>

<body>
    <h1>View All Citations</h1>
    <main class="container">
        <?php

        $system = init();
        foreach ($system->authors as $author) {
            # code...
            echo displayAuthor($author);
        }
        ?>
    </main>

</body>

</html>
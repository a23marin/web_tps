<?php
require("./utils/inputs.php");
include_once("./utils/db-functions.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Citation</title>
    <style type="text/css">
        .citation {
            display: flex;
            flex-direction: column;
            gap: 1rem;
            margin: 1rem 2rem;
            padding: 0.5rem 1rem;
            border: solid 1px black;
            width: fit-content;


        }

        p {
            margin: 0;

        }
    </style>
</head>

<body>
    <?php
    $citationModel = new CitationModel();
    $citation = Citation::toArray($citationModel->findByID(getValue($_GET["id"])));

    ?>
    <h1>View Citation</h1>
    <div class="citation">
        <p>login:
            <b>
                <?php echo $citation["login"] ?>
                <?php // echo getValue($_POST["login"]) ?>
            </b>
        </p>
        <p>
            Citation:
            <b>
                <?php echo $citation["citation"] ?>

                <?php //echo getValue($_POST["citation"]) ?>
            </b>
        </p>
        <p> Auteur:
            <b>
                <?php echo $citation["author"] ?>
                <?php //echo getValue($_POST["auteur"]) ?>
            </b>

        </p>
        <p>
            Date:
            <b>
                <?php echo $citation["date"] ?>
                <?php //echo getValue($_POST["date"]) ?>
            </b>
        </p>
    </div>
</body>

</html>
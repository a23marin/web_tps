<?php

include_once("./Entity/Citation.class.php");
function displayAuthor(Author $author): string
{
    $citationsMarkup = "";
    foreach ($author->citations as $citation) {
        $citationsMarkup .= displayCitation($citation);
    }

    return " 
    <div class=\"box\">
        <h2> Auteur </h2>
        <p> Nom: <b>{$author->lastName} </b></p>
        <p> Pre Nom: <b> {$author->lastName} </b> </p>
        <p> Anne de Naissance: <b>{$author->birthYear}</b></p>
        <div class=\"citations-container\">
            $citationsMarkup
        </div>
    </div>
    \n";
}
;
function displayCitation(Citation $citation): string
{
    $markup = "
    <div class=\"citation\">
        <p> Login: <b>{$citation->login}</b></p>
        <p> Citation: <b> {$citation->citation} </b> </p>
        <p> Date: <b> {$citation->date}</b> </p>
    </div>
    \n";
    return $markup;
}
;

?>
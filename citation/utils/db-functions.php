<?php
include_once 'config/db-config.php';
include_once 'Entity/Citation.class.php';
include_once 'Entity/Author.class.php';

class DBAdapter
{
    static protected ?PDO $pdo = null;

    public function __construct(
        protected string $table,
    )
    {
        DBAdapter::connect();
    }

    public function execute(string $sql, array $params = null)
    {
        $req = DBAdapter::$pdo->prepare($sql);
        $result = $req->execute($params);
        if (!$result) {
            // handle error;
        }
        return $req;
    }

    private static function connect(): void
    {

        if (!is_null(DBAdapter::$pdo)) {
            return;
        }


        global $dbConfig;
        try {

            self::$pdo = new PDO(
                "mysql:host={$dbConfig["host"]}:{$dbConfig["port"]};dbname={$dbConfig["database"]}",
                $dbConfig["user"],
                $dbConfig["password"],
                array(
                        PDO::ATTR_ERRMODE =>
                        PDO::ERRMODE_EXCEPTION
                )
            );
        } catch (PDOException $e) {
            die("Error : " . $e->getMessage());
        }
    }

}


class CitationModel extends DBAdapter
{

    public function __construct()
    {
        parent::__construct("citation");
    }


    public function find(array $params = null)
    {
        $sql = "SELECT * FROM `$this->table` " . isset($params) ?: "WHERE :key = :value";
        $req = $this->execute($sql, $params);

        $result = $req->fetchAll();

        return array_map(fn($item) => Citation::fromArray($item), $result);

    }

    public function findOne(array $params = null)
    {
        $sql = "SELECT * FROM `$this->table` WHERE :key = :value";
        $req = $this->execute($sql, $params);

        $result = $req->fetch();

        return Citation::fromArray($result);
    }

    public function findByID(int $id)
    {
        $sql = "SELECT * FROM `$this->table` WHERE author_id = :id";
        $req = $this->execute($sql, ["id" => $id]);

        $result = $req->fetch();

        return Citation::fromArray($result);

    }
    public function create(Citation $citation)
    {
        $sql = "INSERT INTO citation(`login`,`citation`,`date`) VALUES (':login',':citation',':date')";
        $params = ["login" => $citation->login, "citation" => $citation->citation, "date" => $citation->date];
        return $this->execute($sql, $params);
    }
    public function update()
    {
    }
    public function delete()
    {
    }
}
class AuthorModel extends DBAdapter
{

    public function __construct()
    {
        parent::__construct("author");
    }

    public function find(array $params)
    {
        $sql = "SELECT * FROM `$this->table` " . isset($params) ?: "WHERE :key = :value";
        $req = $this->execute($sql, $params);

        return $req->fetchAll();

    }
    public function findOne(string $sql, array $params = null)
    {
        $sql = "SELECT * FROM `$this->table` WHERE :key = :value";
        $req = $this->execute($sql, $params);

        return $req->fetch();
    }
    public function findByID(int $id)
    {
        $sql = "SELECT * FROM `$this->table` WHERE author_id = :id";
        $req = $this->execute($sql, ["id" => $id]);

        return $req->fetch();
    }
    public function create(Author $author)
    {
        $sql = "INSERT INTO author(`name`,`last_name`,`birth_year`) VALUES (':name',':lastName',':birthYear')";
        $params = ["name" => $author->name, "lastName" => $author->lastName, "birthYear" => $author->birthYear];
        return $this->execute($sql, $params);
    }
    public function update()
    {
    }
    public function delete()
    {
    }
}


?>
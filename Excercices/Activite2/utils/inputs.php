<?php 


function checkValue(string $value)
{
    return isset($value);
}
function isNumber(string $value)
{
    if (checkValue($value)) {
        return is_numeric($value) && is_integer((int)$value);
    }

    return false;
}
function getValue(string $value) {
    if (!checkValue($value)) {
        return;
    }
    $sanitizedValue = htmlspecialchars($value);
    return $sanitizedValue;
}

?>
<?php
require("utils/multi.php");
?>
<?php require("./utils/inputs.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style type="text/css">
        .table-container {
            display: flex;
            flex-direction: column;
            border: solid 1px black;
            width: fit-content;
            padding: 0.5rem 1rem;
        }

        .tables {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem;
        }
    </style>
</head>

<body>
    <h1>Tables Test: </h1>
    <div class="tables">
        <?php

        function generateTable(int $n)
        {
            return "<div class=\"table-container\">" . (string) multiplicationTable($n, 10) . "</div>\n";
        }

        for ($i = 1; $i < 10 + 1; $i++) {
            # code...
            $t = generateTable($i);
            echo $t;
        }
        ?>
    </div>


</body>

</html>
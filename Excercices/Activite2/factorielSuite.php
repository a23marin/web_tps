<?php require("./utils/inputs.php") ?>

<html>

<head>
    <title>Factoriel Suite</title>
</head>

<body>

    <p>numero introduit: </p>
    <?php

    if ((checkValue($_GET["value"])) && isNumber($_GET["value"]) && getValue($_GET["value"]) >= 0) {

        $num = getValue($_GET["value"]);
        $acc = 1;

        for ($x = 0; $x <= $num; $x++) {
            $factorial = $x == 0 ? 1 : $acc * $x;
            $acc = $factorial;

            echo "<p>Factoriel de $x est $factorial </p>";

        }

    } else {
        echo "Il faut que tu renseigné un numero!";
    }
    ?>

</body>

</html>

<?php require("./utils/inputs.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style type="text/css">

.table-container {
    display: flex;
    flex-direction: column;
    border: solid 1px black;
    width: fit-content;
    padding: 0.5rem 1rem;

}


    </style>
</head>
<body>
    <h1>Table Test: </h1>
<?php
require("utils/multi.php");
?>
 <div class="table-container">
<?php
echo multiplicationTable(4, 15);

?>  
 </div>

    
</body>
</html>
